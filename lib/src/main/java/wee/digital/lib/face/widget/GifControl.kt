package wee.digital.lib.face.widget

import android.content.Context
import android.graphics.Rect
import android.util.AttributeSet
import android.util.Size
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.control_gif_layout.view.*
import wee.digital.lib.R

class GifControl : ConstraintLayout {

    private var curRect: Rect? = null

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs) {
        inflate(context, R.layout.control_gif_layout, this)
    }

    fun show(isRed: Boolean) {
        this.visibility = View.VISIBLE
        gifControl_gifContainer.visibility = View.VISIBLE
        if (isRed) {
            gifControl_gif.setImageResource(R.drawable.eye_red)
        } else {
            gifControl_gif.setImageResource(R.drawable.eye)
        }
        applyRect()
    }

    fun hide() {
        this.visibility = View.GONE
        gifControl_gifContainer.visibility = View.GONE
    }

    private fun applyRect() {
        val fRect = curRect ?: return
        gifControl_gifContainer.post {
            gifControl_gifContainer.x = fRect.left.toFloat()
            gifControl_gifContainer.y = fRect.top.toFloat()
            val w = fRect.width()
            val h = fRect.height()
            val size = Size(w, h)
            val params = gifControl_gifContainer.layoutParams
            params.width = size.width
            params.height = size.height
            gifControl_gifContainer.layoutParams = params
            gifControl_gifContainer.scaleX = 2.39f
            gifControl_gifContainer.scaleY = 2.39f
        }
    }

    fun setRect(rect: Rect?) {
        rect ?: return
        curRect = rect
        applyRect()
    }

}