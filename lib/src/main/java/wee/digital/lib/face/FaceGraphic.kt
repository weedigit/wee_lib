/*
 * Copyright 2020 Google LLC. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package wee.digital.lib.face

import android.graphics.*
import androidx.core.graphics.contains
import androidx.core.graphics.toPoint
import com.google.mlkit.vision.face.Face
import com.google.mlkit.vision.face.FaceLandmark
import wee.digital.lib.face.util.Image.getRatio
import wee.digital.lib.face.util.Image.getRectRatio
import wee.digital.lib.face.widget.GraphicOverlayFace

/**
 * Graphic instance for rendering face position, contour, and landmarks within the associated
 * graphic overlay view.
 */
class FaceGraphic constructor(
    private val overlay: GraphicOverlayFace,
    private val face: Face
) {

    private var isInside: Boolean? = null

    private val facePositionPaint = Paint().also {
        it.color = Color.RED
        it.alpha = 70
    }

    private val boxPaints = Paint().also {
        it.color = COLOR
        it.style = Paint.Style.STROKE
        it.strokeWidth = BOX_STROKE_WIDTH
    }

    private val labelPaints = Paint().also {
        it.color = COLOR
        it.style = Paint.Style.FILL
        it.textSize = 60f
    }

    fun checkFaceInside(rectTracking: Rect? = MLKitImp.instance.getRectTracking()): Boolean {
        rectTracking?:return false
        if(isInside!=null) return isInside?:false
        val eyeR = face.getLandmark(FaceLandmark.RIGHT_EYE)?.position?.getRatio()
        val eyeL = face.getLandmark(FaceLandmark.LEFT_EYE)?.position?.getRatio()
        if (eyeL != null && eyeR != null) {
            isInside = rectTracking.contains(overlay.translatePoint(eyeL).toPoint())
                    && rectTracking.contains(overlay.translatePoint(eyeR).toPoint())
            return isInside?:false
        }
        return false
    }

    /** Draws the face annotations for position on the supplied canvas.  */
    fun draw(canvas: Canvas) {
        // Draws a circle at the position of the detected face, with the face's track id below.
        val faceRect = face.getRectRatio()
//        val x = overlay.translateX(faceRect.centerX().toFloat())
//        val y = overlay.translateY(faceRect.centerY().toFloat())
//        val left = x - overlay.scale(faceRect.width() / 2.0f)
//        val top = y - overlay.scale(faceRect.height() / 2.0f)
//        val right = x + overlay.scale(faceRect.width() / 2.0f)
//        val bottom = y + overlay.scale(faceRect.height() / 2.0f)
//        val lineHeight =
//                ID_TEXT_SIZE + BOX_STROKE_WIDTH
//        var yLabelOffset: Float = if (face.trackingId == null) 0f else -lineHeight

        val eyeR = face.getLandmark(FaceLandmark.RIGHT_EYE)?.position?.getRatio()
        val eyeL = face.getLandmark(FaceLandmark.LEFT_EYE)?.position?.getRatio()
//        val nose = face.getLandmark(FaceLandmark.NOSE_BASE)?.position?.getRatio()
//        val mouthL = face.getLandmark(FaceLandmark.MOUTH_LEFT)?.position?.getRatio()
//        val mouthR = face.getLandmark(FaceLandmark.MOUTH_RIGHT)?.position?.getRatio()
        if (eyeL != null && eyeR != null /*&& nose != null && mouthL != null && mouthR != null*/) {
            val radius = overlay.translateRect(faceRect).width()*0.04f
            canvas.drawCircle(overlay.translateX(eyeL.x), overlay.translateY(eyeL.y), radius, facePositionPaint)
            canvas.drawCircle(overlay.translateX(eyeR.x), overlay.translateY(eyeR.y), radius, facePositionPaint)
//            canvas.drawCircle(overlay.translateX(nose.x), overlay.translateY(nose.y), FACE_POSITION_RADIUS, facePositionPaint)
//            canvas.drawCircle(overlay.translateX(mouthL.x), overlay.translateY(mouthL.y), FACE_POSITION_RADIUS, facePositionPaint)
//            canvas.drawCircle(overlay.translateX(mouthR.x), overlay.translateY(mouthR.y), FACE_POSITION_RADIUS, facePositionPaint)
        }
        // DRAW BOX
//        canvas.drawRect(left, top, right, bottom, boxPaints)
//        if (face.trackingId != null) {
//            canvas.drawText(
//                    "ID: " + face.trackingId,
//                    left,
//                    top + yLabelOffset,
//                    labelPaints
//            )
//            yLabelOffset += lineHeight
//        }
    }

    companion object {
        private const val FACE_POSITION_RADIUS = 22.0f
        private const val ID_TEXT_SIZE = 30.0f
        private const val ID_Y_OFFSET = 40.0f
        private const val BOX_STROKE_WIDTH = 5.0f
        private const val NUM_COLORS = 10
        private const val COLOR = Color.BLUE
    }

}
