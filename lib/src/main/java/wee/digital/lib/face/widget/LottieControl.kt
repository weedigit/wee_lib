package wee.digital.lib.face.widget

import android.animation.Animator
import android.content.Context
import android.graphics.Rect
import android.util.AttributeSet
import android.util.Size
import android.view.View
import androidx.constraintlayout.widget.ConstraintLayout
import kotlinx.android.synthetic.main.control_lottie_layout.view.*
import wee.digital.lib.R


class LottieControl : ConstraintLayout, Animator.AnimatorListener {

    private var mRootView: View? = null
    private var isPlaying = false
    private var curRect: Rect? = null
    private var block: () -> Unit = {}

    //---
    constructor(context: Context) : super(context)
    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)
    constructor(context: Context, attrs: AttributeSet, defStyleAttr: Int) : super(context, attrs, defStyleAttr)


    //---
    init {
        if (mRootView == null) {
            mRootView = inflate(context, R.layout.control_lottie_layout, this)
        }
    }

    fun playAnimation(block: () -> Unit) {
        if (isPlaying) return
        isPlaying = true
        show()
        title.visibility = View.GONE
        lottieView.playAnimation()
        lottieView.addAnimatorListener(this)
        this.block = block
    }

    fun clear() {
        title.visibility = View.GONE
        lottieView.clearAnimation()
    }

    fun show(){
        this.visibility = View.VISIBLE
        lottieControl_lottieContainer.visibility = View.VISIBLE
        applyRect()
    }

    fun hide(){
        this.visibility = View.GONE
        lottieControl_lottieContainer.visibility = View.GONE
    }

    private fun applyRect(){
        curRect?:return
        lottieControl_lottieContainer.post {
            lottieControl_lottieContainer.x = curRect!!.left.toFloat()
            lottieControl_lottieContainer.y = curRect!!.top.toFloat()
            val w = curRect!!.width()
            val h = curRect!!.height()
            val size = Size(w, h)
            val params = lottieControl_lottieContainer.layoutParams
            params.width = size.width
            params.height = size.height
            lottieControl_lottieContainer.layoutParams = params
            lottieControl_lottieContainer.scaleX = 2.5f
            lottieControl_lottieContainer.scaleY = 2.5f
        }

    }

    fun setRect(rect: Rect?) {
        rect ?: return
//        if(rect==curRect) return
        curRect = rect
        applyRect()
    }

    override fun onAnimationStart(p0: Animator?) {

    }

    override fun onAnimationEnd(p0: Animator?) {
        isPlaying = false
        title.visibility = View.VISIBLE
        this.block()
        this.block = {}
    }

    override fun onAnimationCancel(p0: Animator?) {
        isPlaying = false
    }

    override fun onAnimationRepeat(p0: Animator?) {

    }
}