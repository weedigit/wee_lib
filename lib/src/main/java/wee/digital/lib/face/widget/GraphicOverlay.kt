/*
 * Copyright 2020 Google LLC. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package wee.digital.lib.face.widget

import android.animation.ValueAnimator
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.Log
import android.view.View
import androidx.core.graphics.toRectF
import com.google.android.gms.common.internal.Preconditions
import wee.digital.lib.R
import wee.digital.lib.face.MLKitImp
import java.util.*
import kotlin.math.roundToInt

class GraphicOverlay(context: Context?, attrs: AttributeSet?) : View(context, attrs),
        ValueAnimator.AnimatorUpdateListener {

    var isUpdateImageSource = true
    var rectTracking: Rect = Rect(0,0,0,0)
    var curRect: Rect? = null
    var isCurInside: Boolean? = null
    var isCurOke: Boolean? = null
    var isActionDone: Boolean = false
    var isHide = false


    // Matrix for transforming from image coordinates to overlay view coordinates.
    private val transformationMatrix = Matrix()
    private var imageWidth = 0
    private var imageHeight = 0
    private val corner = 24f.toFloat()

    // The factor of overlay View size to image size. Anything in the image coordinates need to be
    // scaled by this amount to fit with the area of overlay View.
    private var scaleFactor = 1.0f

    // The number of horizontal pixels needed to be cropped on each side to fit the image with the
    // area of overlay View after scaling.
    private var postScaleWidthOffset = 0f

    // The number of vertical pixels needed to be cropped on each side to fit the image with the
    // area of overlay View after scaling.
    private var postScaleHeightOffset = 0f
    private var isImageFlipped = false
    private var needUpdateTransformation = true

    private val paint = Paint().also {
        it.color = Color.TRANSPARENT
        it.xfermode = PorterDuffXfermode(PorterDuff.Mode.CLEAR)
    }
    private val paintDone = Paint().also {
        it.color = Color.GREEN
        it.style = Paint.Style.FILL
    }
    private val paintBorder = Paint().also {
        it.color = Color.RED
        it.strokeWidth = 10f
        it.style = Paint.Style.STROKE
    }

    private val runUpdateUI = Runnable {
        postInvalidate()
    }


    fun drawBG(isInside: Boolean, isOke: Boolean) {
        synchronized(rectTracking) {
            if (this.isCurInside != isInside || this.isCurOke != isOke) {
                this.isCurInside = isInside
                this.isCurOke = isOke
                Log.i("DrawBG", "runUpdateUI")
                this.handler.removeCallbacks(runUpdateUI)
                this.handler.postDelayed(runUpdateUI, 400)
            }
        }
    }

    /**
     * Sets the source information of the image being processed by detectors, including size and
     * whether it is flipped, which informs how to transform image coordinates later.
     *
     * @param imageWidth the width of the image sent to ML Kit detectors
     * @param imageHeight the height of the image sent to ML Kit detectors
     * @param isFlipped whether the image is flipped. Should set it to true when the image is from the
     * front camera.
     */
    fun setImageSourceInfo(imageWidth: Int, imageHeight: Int, isFlipped: Boolean) {
        if (!isUpdateImageSource) return
        Preconditions.checkState(imageWidth > 0, "image width must be positive")
        Preconditions.checkState(imageHeight > 0, "image height must be positive")
        synchronized(rectTracking) {
            this.imageWidth = imageWidth
            this.imageHeight = imageHeight
            isImageFlipped = isFlipped
            needUpdateTransformation = true
        }
        postInvalidate()
        isUpdateImageSource = false
    }

    fun updateImageSourceInfo() {
        isUpdateImageSource = true
    }

    private fun updateTransformationIfNeeded() {
        if (!needUpdateTransformation || imageWidth <= 0 || imageHeight <= 0) {
            return
        }
        val viewAspectRatio = width.toFloat() / height
        val imageAspectRatio = imageWidth.toFloat() / imageHeight
        postScaleWidthOffset = 0f
        postScaleHeightOffset = 0f
        if (viewAspectRatio > imageAspectRatio) {
            // The image needs to be vertically cropped to be displayed in this view.
            scaleFactor = width.toFloat() / imageWidth
            postScaleHeightOffset = (width.toFloat() / imageAspectRatio - height) / 2
        } else {
            // The image needs to be horizontally cropped to be displayed in this view.
            scaleFactor = height.toFloat() / imageHeight
            postScaleWidthOffset = (height.toFloat() * imageAspectRatio - width) / 2
        }
        transformationMatrix.reset()
        transformationMatrix.setScale(scaleFactor, scaleFactor)
        transformationMatrix.postTranslate(-postScaleWidthOffset, -postScaleHeightOffset)
        if (isImageFlipped) {
            transformationMatrix.postScale(-1f, 1f, width / 2f, height / 2f)
        }
        this.rectTracking = translateRect(MLKitImp.instance.getRawRectTracking(imageWidth, imageHeight))
        Log.e("CurRect","updateTransformationIfNeeded ${this.rectTracking}")
        needUpdateTransformation = false
    }

    private var actionWidth: Int = -1
    private var curActionRect: Rect? = null
    private var mAnimator: ValueAnimator? = null

    fun hideBG() {
        synchronized(rectTracking) {
            isHide = true
        }
        postInvalidate()
    }

    fun showBG() {
        synchronized(rectTracking) {
            isHide = false
        }
        postInvalidate()
    }

    fun startActionDone() {
        rectTracking ?: return
        isActionDone = true
        curActionRect = rectTracking
        actionWidth = rectTracking!!.width() - rectTracking!!.height()
        mAnimator = ValueAnimator.ofInt(rectTracking!!.width(), rectTracking!!.height())
        mAnimator!!.duration = 1000
        mAnimator!!.addUpdateListener(this)
        mAnimator!!.start()
    }

    fun setValueActionDone(value: Float) {
        val change = ((value * actionWidth) / 2).roundToInt()
        val l = rectTracking!!.left + change
        val t = rectTracking!!.top
        val r = rectTracking!!.right - change
        val b = rectTracking!!.bottom
        curActionRect = Rect(l, t, r, b)
        postInvalidate()
    }

    private fun drawActionDone(canvas: Canvas, rectDraw: Rect? = this.curActionRect) {
        if (rectDraw != null) {
            canvas.drawRoundRect(rectDraw.toRectF(), actionWidth.toFloat(), actionWidth.toFloat(), paintDone)
        }
    }

    private fun drawBGWhite(canvas: Canvas, rectTracking: Rect? = this.rectTracking) {
        Log.e("CurRect","drawBGWhite ${this.rectTracking}")
        if (rectTracking != null) {
            paintBorder.color = if (isCurOke == true) {
                Color.BLACK
            } else {
                Color.YELLOW
            }
//            canvas.drawColor(resources.getColor(R.color.white, null))
            paintBorder.color = Color.GREEN
            val rectDraw = rectTracking.toRectF()
            canvas.drawRoundRect(rectDraw, corner, corner, paint)
            canvas.drawRoundRect(rectDraw, corner, corner, paintBorder)
        }

    }

    private fun drawBGTransparent(canvas: Canvas, rectTracking: Rect? = this.rectTracking) {
        Log.e("CurRect","drawBGTransparent ${this.rectTracking}")
        if (rectTracking != null) {
//            canvas.drawColor(resources.getColor(R.color.overlay, null))
            val rectDraw = rectTracking.toRectF()
            paintBorder.color = Color.GREEN
            canvas.drawRoundRect(rectDraw, corner, corner, paint)
            canvas.drawRoundRect(rectDraw, corner, corner, paintBorder)
        }
    }

    /** Draws the overlay with its associated graphic objects.  */
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        synchronized(rectTracking) {
            updateTransformationIfNeeded()
            if (!isHide) {
                if (!isActionDone) {
                    if (isCurInside == true) {
                        drawBGWhite(canvas)
                    } else {
                        drawBGTransparent(canvas)
                    }
                } else {
                    drawActionDone(canvas)
                }
            }

        }
    }

    override fun onAnimationUpdate(value: ValueAnimator?) {
        Log.e("onAnimationUpdate", "${value?.animatedFraction}")
        setValueActionDone(value?.animatedFraction ?: 0f)
    }

    /** Adjusts the supplied value from the image scale to the view scale.  */
    fun scale(imagePixel: Float): Float {
        return imagePixel * this.scaleFactor
    }

    /**
     * Adjusts the x coordinate from the image's coordinate system to the view coordinate system.
     */
    fun translateX(x: Float): Float {
        return if (this.isImageFlipped) {
            this.width - (scale(x) - this.postScaleWidthOffset)
        } else {
            scale(x) - this.postScaleWidthOffset
        }
    }

    /**
     * Adjusts the y coordinate from the image's coordinate system to the view coordinate system.
     */
    fun translateY(y: Float): Float {
        return scale(y) - this.postScaleHeightOffset
    }

    fun translateRect(rect: Rect): Rect {
        val top = translateY(rect.top.toFloat()).roundToInt()
        val left = translateX(rect.left.toFloat()).roundToInt()
        val right = translateX(rect.right.toFloat()).roundToInt()
        val bottom = translateY(rect.bottom.toFloat()).roundToInt()
        return Rect(left, top, right, bottom)
    }

    fun translatePoint(point: PointF): PointF {
        val x = translateX(point.x)
        val y = translateY(point.y)
        return PointF(x, y)
    }

    init {
        addOnLayoutChangeListener { view: View?, left: Int, top: Int, right: Int, bottom: Int, oldLeft: Int, oldTop: Int, oldRight: Int, oldBottom: Int -> needUpdateTransformation = true }
    }

}