package wee.digital.lib.face.util

import android.content.Context
import org.opencv.android.BaseLoaderCallback
import org.opencv.android.LoaderCallbackInterface
import org.opencv.android.OpenCVLoader

/**
 * -------------------------------------------------------------------------------------------------
 * @Project: WeeLib
 * @Created: Huy 2021/05/07
 * @Organize: Wee Digital
 * @Description: ...
 * All Right Reserved
 * -------------------------------------------------------------------------------------------------
 */
object OpenCV {

    var openCVInitialized = false

    fun init(context: Context) {
        if (openCVInitialized) return
        val loaderCallback = object : BaseLoaderCallback(context) {
            override fun onManagerConnected(status: Int) {
                when (status) {
                    LoaderCallbackInterface.SUCCESS -> {
                        openCVInitialized = true
                    }
                    else -> {
                        openCVInitialized = false
                        super.onManagerConnected(status)
                    }
                }
            }
        }
        if (!OpenCVLoader.initDebug()) {
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION, context, loaderCallback)
        } else {
            loaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS)
        }
    }
}