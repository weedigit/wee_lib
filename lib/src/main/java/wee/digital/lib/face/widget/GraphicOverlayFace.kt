/*
 * Copyright 2020 Google LLC. All rights reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package wee.digital.lib.face.widget

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.View
import com.google.android.gms.common.internal.Preconditions
import wee.digital.lib.face.MLKitImp
import wee.digital.lib.face.FaceGraphic
import java.util.*
import kotlin.math.roundToInt

/**
 * A view which renders a series of custom graphics to be overlayed on top of an associated preview
 * (i.e., the camera preview). The creator can add graphics objects, update the objects, and remove
 * them, triggering the appropriate drawing and invalidation within the view.
 *
 *
 * Supports scaling and mirroring of the graphics relative the camera's preview properties. The
 * idea is that detection items are expressed in terms of an image size, but need to be scaled up
 * to the full view size, and also mirrored in the case of the front-facing camera.
 *
 *
 * Associated [Graphic] items should use the following methods to convert to view
 * coordinates for the graphics that are drawn:
 *
 *
 *  1. [Graphic.scale] adjusts the size of the supplied value from the image scale
 * to the view scale.
 *  1. [Graphic.translateX] and [Graphic.translateY] adjust the
 * coordinate from the image's coordinate system to the view coordinate system.
 *
 */
class GraphicOverlayFace(context: Context?, attrs: AttributeSet?) : View(context, attrs) {

    private val lock = Any()
    private val graphics: MutableList<FaceGraphic> = ArrayList()

    var isUpdateImageSource = true
    var rectTracking: Rect? = null
    var isInside = false

    // Matrix for transforming from image coordinates to overlay view coordinates.
    val transformationMatrix = Matrix()
    var imageWidth = 0
    var imageHeight = 0

    // The factor of overlay View size to image size. Anything in the image coordinates need to be
    // scaled by this amount to fit with the area of overlay View.
    var scaleFactor = 1.0f

    // The number of horizontal pixels needed to be cropped on each side to fit the image with the
    // area of overlay View after scaling.
    var postScaleWidthOffset = 0f

    // The number of vertical pixels needed to be cropped on each side to fit the image with the
    // area of overlay View after scaling.
    var postScaleHeightOffset = 0f
    var isImageFlipped = false
    var needUpdateTransformation = true


    /** Removes all graphics from the overlay.  */
    fun clear() {
        synchronized(lock) { graphics.clear() }
        postInvalidate()
    }

    /** Adds a graphic to the overlay.  */
    fun add(graphic: FaceGraphic) {
        synchronized(lock) { graphics.add(graphic) }
    }

    /** Removes a graphic from the overlay.  */
    fun remove(graphic: FaceGraphic) {
        synchronized(lock) { graphics.remove(graphic) }
        postInvalidate()
    }

    /**
     * Sets the source information of the image being processed by detectors, including size and
     * whether it is flipped, which informs how to transform image coordinates later.
     *
     * @param imageWidth the width of the image sent to ML Kit detectors
     * @param imageHeight the height of the image sent to ML Kit detectors
     * @param isFlipped whether the image is flipped. Should set it to true when the image is from the
     * front camera.
     */
    fun setImageSourceInfo(imageWidth: Int, imageHeight: Int, isFlipped: Boolean) {
        if (!isUpdateImageSource) return
        Preconditions.checkState(imageWidth > 0, "image width must be positive")
        Preconditions.checkState(imageHeight > 0, "image height must be positive")
        synchronized(lock) {
            this.imageWidth = imageWidth
            this.imageHeight = imageHeight
            isImageFlipped = isFlipped
            needUpdateTransformation = true
        }
        postInvalidate()
        isUpdateImageSource = false
    }

    fun updateImageSourceInfo() {
        isUpdateImageSource = true
    }

    private fun updateTransformationIfNeeded() {
        if (!needUpdateTransformation || imageWidth <= 0 || imageHeight <= 0) {
            return
        }
        val viewAspectRatio = width.toFloat() / height
        val imageAspectRatio = imageWidth.toFloat() / imageHeight
        postScaleWidthOffset = 0f
        postScaleHeightOffset = 0f
        if (viewAspectRatio > imageAspectRatio) {
            // The image needs to be vertically cropped to be displayed in this view.
            scaleFactor = width.toFloat() / imageWidth
            postScaleHeightOffset = (width.toFloat() / imageAspectRatio - height) / 2
        } else {
            // The image needs to be horizontally cropped to be displayed in this view.
            scaleFactor = height.toFloat() / imageHeight
            postScaleWidthOffset = (height.toFloat() * imageAspectRatio - width) / 2
        }
        transformationMatrix.reset()
        transformationMatrix.setScale(scaleFactor, scaleFactor)
        transformationMatrix.postTranslate(-postScaleWidthOffset, -postScaleHeightOffset)
        if (isImageFlipped) {
            transformationMatrix.postScale(-1f, 1f, width / 2f, height / 2f)
        }
        this.rectTracking = MLKitImp.instance.getRectTracking(imageWidth, imageHeight)

        needUpdateTransformation = false
    }

    /** Draws the overlay with its associated graphic objects.  */
    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        synchronized(lock) {
            updateTransformationIfNeeded()
            if (graphics.isNotEmpty()) {
                val faceGraphic = graphics.first()
                if (!faceGraphic.checkFaceInside()) {
                    faceGraphic.draw(canvas)
                }
            }
        }
    }

    /** Adjusts the supplied value from the image scale to the view scale.  */
    fun scale(imagePixel: Float): Float {
        return imagePixel * this.scaleFactor
    }

    /**
     * Adjusts the x coordinate from the image's coordinate system to the view coordinate system.
     */
    fun translateX(x: Float): Float {
        return if (this.isImageFlipped) {
            this.width - (scale(x) - this.postScaleWidthOffset)
        } else {
            scale(x) - this.postScaleWidthOffset
        }
    }

    /**
     * Adjusts the y coordinate from the image's coordinate system to the view coordinate system.
     */
    fun translateY(y: Float): Float {
        return scale(y) - this.postScaleHeightOffset
    }

    fun translateRect(rect: Rect): Rect {
        val top = translateY(rect.top.toFloat()).roundToInt()
        val left = translateX(rect.left.toFloat()).roundToInt()
        val right = translateX(rect.right.toFloat()).roundToInt()
        val bottom = translateY(rect.bottom.toFloat()).roundToInt()
        return Rect(left, top, right, bottom)
    }

    fun translatePoint(point: PointF): PointF {
        val x = translateX(point.x)
        val y = translateY(point.y)
        return PointF(x, y)
    }

    init {
        addOnLayoutChangeListener { view: View?, left: Int, top: Int, right: Int, bottom: Int, oldLeft: Int, oldTop: Int, oldRight: Int, oldBottom: Int -> needUpdateTransformation = true }
    }
}