package wee.digital.lib.face.util

import android.app.ActivityManager
import android.content.Context
import com.google.android.gms.tasks.Task
import com.google.android.gms.tasks.TaskExecutors
import com.google.mlkit.vision.face.Face
import wee.digital.lib.WeeLib
import wee.digital.lib.face.ScopedExecutor
import java.util.*

/**
 * -------------------------------------------------------------------------------------------------
 * @Project: WeeLib
 * @Created: Huy 2021/05/07
 * @Organize: Wee Digital
 * @Description: ...
 * All Right Reserved
 * -------------------------------------------------------------------------------------------------
 */
object FaceRegLogger {

    val log = Logger("FaceReg")

    // Used to calculate latency, running in the same thread, no sync needed.
    private var numRuns = 0
    private var totalFrameMs = 0L
    private var maxFrameMs = 0L
    private var minFrameMs = Long.MAX_VALUE
    private var totalDetectorMs = 0L
    private var maxDetectorMs = 0L
    private var minDetectorMs = Long.MAX_VALUE
    private var detectingTimeStarted: Long = 0

    // Frame count that have been processed so far in an one second interval to calculate FPS.
    private var frameProcessedInOneSecondInterval = 0
    private var framesPerSecond = 0

    private val fpsTimer = Timer().also {
        it.scheduleAtFixedRate(
            object : TimerTask() {
                override fun run() {
                    framesPerSecond = frameProcessedInOneSecondInterval
                    frameProcessedInOneSecondInterval = 0
                }
            },
            0,
            1000
        )
    }

    private val activityManager = WeeLib.app.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager

    private val executor = ScopedExecutor(TaskExecutors.MAIN_THREAD)

    fun logRequestDetectInImage(frameStartTime: Long, task: Task<List<Face>>) {
        detectingTimeStarted = Logger.now
        task
        /*.addOnSuccessListener(executor) { results: List<Face> ->
            onDetectSuccess(frameStartTime)
        }
        .addOnFailureListener(executor) { e: Exception ->
            onDetectFailed(e)
        }*/
    }

    fun onDetectStart() {
        detectingTimeStarted = Logger.now
    }

    fun onDetectSuccess(frameStartTime: Long) {
        val detectingTimeEnd = Logger.now
        val currentFrameLatencyMs = detectingTimeEnd - frameStartTime
        val currentDetectorLatencyMs = detectingTimeEnd - detectingTimeStarted
        if (numRuns >= 500) {
            resetLatencyStats()
        }
        numRuns++
        frameProcessedInOneSecondInterval++
        totalFrameMs += currentFrameLatencyMs
        maxFrameMs = currentFrameLatencyMs.coerceAtLeast(maxFrameMs)
        minFrameMs = currentFrameLatencyMs.coerceAtMost(minFrameMs)
        totalDetectorMs += currentDetectorLatencyMs
        maxDetectorMs = currentDetectorLatencyMs.coerceAtLeast(maxDetectorMs)
        minDetectorMs = currentDetectorLatencyMs.coerceAtMost(minDetectorMs)
        // Only log inference info once per second. When frameProcessedInOneSecondInterval is
        // equal to 1, it means this is the first frame processed during the current second.
        if (frameProcessedInOneSecondInterval == 1) {
            log.d("Num of Runs: $numRuns")
            log.d("Frame latency: max=$maxFrameMs, min=$minFrameMs, avg=" + (totalFrameMs / numRuns))
            log.d("Detector latency: max=$maxDetectorMs, min=$minDetectorMs, avg=" + (totalDetectorMs / numRuns))
            val mi = ActivityManager.MemoryInfo()
            activityManager.getMemoryInfo(mi)
            val availableMegs = mi.availMem / 0x100000L
            log.d("Memory available in system: $availableMegs MB")
        }
    }

    fun onDetectFailed(e: Exception) {
        log.e("Failed to process: ${e.localizedMessage}\nCause: ${e.cause}")
    }

    fun resetLatencyStats() {
        numRuns = 0
        totalFrameMs = 0
        maxFrameMs = 0
        minFrameMs = Long.MAX_VALUE
        totalDetectorMs = 0
        maxDetectorMs = 0
        minDetectorMs = Long.MAX_VALUE
    }

    fun cancelFpsTimer() {
        fpsTimer.cancel()
    }

}