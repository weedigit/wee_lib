package wee.digital.lib.face.util

import android.graphics.Bitmap
import android.graphics.ImageFormat
import android.graphics.PointF
import android.graphics.Rect
import android.media.Image
import android.util.Log
import com.google.mlkit.vision.face.Face
import org.opencv.android.Utils
import org.opencv.core.*
import org.opencv.core.CvType.CV_8UC1
import org.opencv.imgproc.Imgproc
import org.opencv.imgproc.Imgproc.cvtColor
import wee.digital.lib.face.MLKitImp
import wee.digital.lib.face.MLKitImp.Companion.RESIZE_BLURRY
import wee.digital.lib.face.MLKitImp.Companion.RESIZE_BRIGHT
import kotlin.math.pow
import kotlin.math.roundToInt


object Image {

    fun ByteArray?.nv21ToMat(width: Int, height: Int): Mat{
        this ?: return Mat()
        if(width==0) return Mat()
        if(height==0) return Mat()
        return try {
            val yuv = Mat(height + height / 2, width, CV_8UC1)
            yuv.put(0, 0, this)
            val rgb = Mat()
            cvtColor(yuv, rgb, Imgproc.COLOR_YUV2RGBA_NV21, 4)
            yuv.release()
            return rgb
        } catch (e: Exception) {
            e.printStackTrace()
            Mat()
        }
    }

    fun Mat.rotate(rotate: Int = Core.ROTATE_90_COUNTERCLOCKWISE, isFlip: Boolean = true): Mat {
        val dst = Mat()
        Core.rotate(this, dst, Core.ROTATE_90_COUNTERCLOCKWISE)
        if(isFlip) Core.flip(dst, dst, 1) // Mirror
        this.release()
        return dst
    }

    fun Mat.resizeFullFrame(resize: Double = MLKitImp.RESIZE): Mat? {
        return try {
            if (MLKitImp.RATIO == 0.0) {
                val ratio = resize / this.width().coerceAtLeast(this.height())
                MLKitImp.RATIO = ratio
            }
            val downscaledSize = Size(this.width() * MLKitImp.RATIO, this.height() * MLKitImp.RATIO)
            val srcResize = Mat(downscaledSize, this.type())
            Imgproc.resize(this, srcResize, downscaledSize)
            srcResize
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    fun Mat.matResize(resize: Double = MLKitImp.RESIZE): Mat? {
        return try {
            val ratio = resize / this.width().coerceAtLeast(this.height())
            val downscaledSize = Size(this.width() * ratio, this.height() * ratio)
            val srcResize = Mat(downscaledSize, this.type())
            Imgproc.resize(this, srcResize, downscaledSize)
            srcResize
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    fun Mat.toBitmap(): Bitmap? {
        return try {
            val bitmap = Bitmap.createBitmap(this.cols(), this.rows(), Bitmap.Config.ARGB_8888)
            Utils.matToBitmap(this, bitmap)
            this.release()
            bitmap
        } catch (e: Exception) {
            null
        }

    }

    fun Mat.cropRect(rect: Rect): Mat {
        val w = this.cols()
        val h = this.rows()
        val rectCrop = rect.getRectCrop(w, h).toOpenCVRoi()
        val cropped = Mat(this, rectCrop)
        this.release()
        return cropped
    }

    fun Face.getRectRatio(ratio: Double = MLKitImp.RATIO): Rect {
        val rect = this.boundingBox
        val left = rect.left / ratio
        val top = rect.top / ratio
        val right = rect.right / ratio
        val bottom = rect.bottom / ratio
        return Rect(left.roundToInt(), top.roundToInt(), right.roundToInt(), bottom.roundToInt())
    }

    fun PointF.getRatio(ratio: Double = MLKitImp.RATIO): PointF {
        val x = this.x / ratio.toFloat()
        val y = this.y / ratio.toFloat()
        return PointF(x, y)
    }

    fun Rect.getRectCrop(w: Int, h: Int): Rect {
        val top = if (top < 0) 0 else top
        val left = if (left < 0) 0 else left
        val right = if (right > w) w else right
        val bottom = if (bottom > h) h else bottom
        return Rect(left, top, right, bottom)
    }

    fun Rect.toOpenCVRoi(): org.opencv.core.Rect {
        return org.opencv.core.Rect(left, top, this.width(), this.height())
    }

    fun checkBrightness(mat: Mat?): Double {
        mat ?: return 0.0
        val src = mat.matResize(RESIZE_BRIGHT)
        src ?: return 0.0
        val w = src.cols()
        val h = src.rows()
        val minus = (w * 0.28).roundToInt()
        val l = minus
        val t = minus
        val r = w - minus
        val b = h - minus
        val rectcrop = Rect(l, t, r, b)
        val cropSrc = src.cropRect(rectcrop)
        val gray = Mat()
        cvtColor(cropSrc, gray, Imgproc.COLOR_BGR2GRAY)
        val value = Core.mean(gray).`val`[0]
        src.release()
        gray.release()
        mat.release()
        cropSrc.release()
        return value
    }

    fun checkIfImageIsBlurred(mat: Mat?): Double {
        if (mat == null) {
            return 0.0
        }
        val src = mat.matResize(RESIZE_BLURRY)
        src ?: return 0.0
        val w = src.cols()
        val h = src.rows()
        val minus = (w * 0.28).roundToInt()
        val l = minus
        val t = minus
        val r = w - minus
        val b = h - minus
        val rectcrop = Rect(l, t, r, b)
        val cropSrc = src.cropRect(rectcrop)
        val gray = Mat()
        cvtColor(cropSrc, gray, Imgproc.COLOR_RGB2GRAY)

        val postLaplacianMat = Mat()
        Imgproc.Laplacian(gray, postLaplacianMat, 5)

        val mean = MatOfDouble()
        val standardDeviation = MatOfDouble()
        Core.meanStdDev(postLaplacianMat, mean, standardDeviation)

        val result = standardDeviation.get(0, 0)[0].pow(2.0)
        Log.e("blurryResult", "" + result)
        gray.release()
        postLaplacianMat.release()
        standardDeviation.release()
        src.release()
        mat.release()
        cropSrc.release()
        return result
    }

    fun Image.yuvToRgba(): Mat {
        val rgbaMat = Mat()

        if (format == ImageFormat.YUV_420_888
                && planes.size == 3) {

            val chromaPixelStride = planes[1].pixelStride

            if (chromaPixelStride == 2) { // Chroma channels are interleaved
                assert(planes[0].pixelStride == 1)
                assert(planes[2].pixelStride == 2)
                val yPlane = planes[0].buffer
                val uvPlane1 = planes[1].buffer
                val uvPlane2 = planes[2].buffer
                val yMat = Mat(height, width, CV_8UC1, yPlane)
                val uvMat1 = Mat(height / 2, width / 2, CvType.CV_8UC2, uvPlane1)
                val uvMat2 = Mat(height / 2, width / 2, CvType.CV_8UC2, uvPlane2)
                val addrDiff = uvMat2.dataAddr() - uvMat1.dataAddr()
                if (addrDiff > 0) {
                    assert(addrDiff == 1L)
                    Imgproc.cvtColorTwoPlane(yMat, uvMat1, rgbaMat, Imgproc.COLOR_YUV2RGBA_NV12)
                } else {
                    assert(addrDiff == -1L)
                    Imgproc.cvtColorTwoPlane(yMat, uvMat2, rgbaMat, Imgproc.COLOR_YUV2RGBA_NV21)
                }
            } else { // Chroma channels are not interleaved
                val yuvBytes = ByteArray(width * (height + height / 2))
                val yPlane = planes[0].buffer
                val uPlane = planes[1].buffer
                val vPlane = planes[2].buffer

                yPlane.get(yuvBytes, 0, width * height)

                val chromaRowStride = planes[1].rowStride
                val chromaRowPadding = chromaRowStride - width / 2

                var offset = width * height
                if (chromaRowPadding == 0) {
                    // When the row stride of the chroma channels equals their width, we can copy
                    // the entire channels in one go
                    uPlane.get(yuvBytes, offset, width * height / 4)
                    offset += width * height / 4
                    vPlane.get(yuvBytes, offset, width * height / 4)
                } else {
                    // When not equal, we need to copy the channels row by row
                    for (i in 0 until height / 2) {
                        uPlane.get(yuvBytes, offset, width / 2)
                        offset += width / 2
                        if (i < height / 2 - 1) {
                            uPlane.position(uPlane.position() + chromaRowPadding)
                        }
                    }
                    for (i in 0 until height / 2) {
                        vPlane.get(yuvBytes, offset, width / 2)
                        offset += width / 2
                        if (i < height / 2 - 1) {
                            vPlane.position(vPlane.position() + chromaRowPadding)
                        }
                    }
                }

                val yuvMat = Mat(height + height / 2, width, CV_8UC1)
                yuvMat.put(0, 0, yuvBytes)
                cvtColor(yuvMat, rgbaMat, Imgproc.COLOR_YUV2RGBA_I420, 4)
            }
        }

        return rgbaMat
    }

}