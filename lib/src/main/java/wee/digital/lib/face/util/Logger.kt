package wee.digital.lib.face.util

import android.os.SystemClock
import android.util.Log
import com.google.mlkit.vision.face.Face
import wee.digital.lib.face.Landmark

/**
 * -------------------------------------------------------------------------------------------------
 * @Project: ExLenFacingControl
 * @Created: Huy 2021/04/09
 * @Organize: Wee Digital
 * @Description: ...
 * All Right Reserved
 * -------------------------------------------------------------------------------------------------
 */
class Logger(private val TAG: String = "Face") {

    companion object {
        val now: Long get() = SystemClock.elapsedRealtime()
    }

    fun e(s: String?) {
        Log.e(TAG, s.toString())
    }

    fun d(s: String?) {
        Log.d(TAG, s.toString())
    }

    fun v(s: String?) {
        Log.v(TAG, s.toString())
    }

    fun log(face: Face) {
        val s = StringBuilder()
        s.append("Face:")
        s.append("\nBounding box: ${face.boundingBox.flattenToString()}")
        s.append("Euler Angle X: ${face.headEulerAngleX}")
        s.append("Euler Angle Y:  ${face.headEulerAngleY}")
        s.append("Euler Angle Z: ${face.headEulerAngleZ}")
        Landmark.listEyes.iterator().forEach {
            when (val landmark = face.getLandmark(it.type)) {
                null -> {
                    s.append("${it.name}: landmark not found")
                }
                else -> {
                    val landmarkPosition = landmark.position
                    s.append("${it.name}: x: %f , y: %f".format(landmarkPosition.x, landmarkPosition.y))
                }
            }
        }
        s.append("face left eye open probability: ${face.leftEyeOpenProbability}")
        s.append("face right eye open probability: ${face.rightEyeOpenProbability}")
        s.append("face smiling probability: ${face.smilingProbability}")
        s.append("face tracking id: ${face.trackingId}")
        d(s.toString())
    }


}