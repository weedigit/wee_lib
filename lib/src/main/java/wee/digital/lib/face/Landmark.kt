package wee.digital.lib.face

import com.google.mlkit.vision.face.FaceLandmark

/**
 * -------------------------------------------------------------------------------------------------
 * @Project: ExLenFacingControl
 * @Created: Huy 2021/04/08
 * @Organize: Wee Digital
 * @Description: ...
 * All Right Reserved
 * -------------------------------------------------------------------------------------------------
 */
class Landmark constructor(val type: Int, val name: String) {

    companion object {

        val listAll = listOf(
            Landmark(FaceLandmark.MOUTH_BOTTOM, "MOUTH_BOTTOM"),
            Landmark(FaceLandmark.MOUTH_RIGHT, "MOUTH_RIGHT"),
            Landmark(FaceLandmark.MOUTH_LEFT, "MOUTH_LEFT"),
            Landmark(FaceLandmark.RIGHT_EYE, "RIGHT_EYE"),
            Landmark(FaceLandmark.LEFT_EYE, "LEFT_EYE"),
            Landmark(FaceLandmark.RIGHT_EAR, "RIGHT_EAR"),
            Landmark(FaceLandmark.LEFT_EAR, "LEFT_EAR"),
            Landmark(FaceLandmark.RIGHT_CHEEK, "RIGHT_CHEEK"),
            Landmark(FaceLandmark.LEFT_CHEEK, "LEFT_CHEEK"),
            Landmark(FaceLandmark.NOSE_BASE, "NOSE_BASE")
        )

        val listEyes = listOf(
            Landmark(FaceLandmark.RIGHT_EYE, "RIGHT_EYE"),
            Landmark(FaceLandmark.LEFT_EYE, "LEFT_EYE")
        )

    }
}