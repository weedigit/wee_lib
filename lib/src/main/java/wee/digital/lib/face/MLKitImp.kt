package wee.digital.lib.face

import android.graphics.Rect
import android.media.Image
import android.util.Size
import com.google.mlkit.vision.common.InputImage
import com.google.mlkit.vision.face.Face
import com.google.mlkit.vision.face.FaceDetection
import com.google.mlkit.vision.face.FaceDetectorOptions
import org.opencv.core.Mat
import wee.digital.lib.face.util.Image.nv21ToMat
import wee.digital.lib.face.util.Image.resizeFullFrame
import wee.digital.lib.face.util.Image.rotate
import wee.digital.lib.face.util.Image.toBitmap
import wee.digital.lib.face.util.Image.yuvToRgba
import wee.digital.lib.face.widget.GraphicOverlay
import wee.digital.lib.face.widget.GraphicOverlayFace
import java.util.concurrent.Executors
import kotlin.math.roundToInt


class MLKitImp {

    companion object {
        val instance by lazy {
            MLKitImp()
        }
        const val MIN_SIZE = 30
        const val MIN_BLUR = 250.0
        const val RESIZE = 120.0
        const val RESIZE_BLURRY = 200.0
        const val RESIZE_BRIGHT = 200.0

        const val MIN = -8f
        const val MAX = 8f
        private const val MINZ = -5f
        const val MAXZ = 5f
        val ZONE_X = MIN..MAX
        val ZONE_Y = MIN..MAX
        val ZONE_Z = MINZ..MAXZ
        var RATIO = 0.0
    }

    private var trackingRect: Rect? = null
    private var graphicOverlayFace: GraphicOverlayFace? = null
    private var graphicOverlayBG: GraphicOverlay? = null
    private var SIZE = Size(720, 1280)

    fun getRectTracking(width: Int = SIZE.width, height: Int = SIZE.height): Rect? {
        if (trackingRect != null) return trackingRect
        graphicOverlayFace ?: return null
        if (width == 0 || height == 0) return null
        val rect = getRawRectTracking(width, height)
        trackingRect = graphicOverlayFace!!.translateRect(rect)
        return trackingRect
    }

    fun getRawRectTracking(width: Int = SIZE.width, height: Int = SIZE.height): Rect {
        val l = (width * 0.22).roundToInt()
        val t = (height * 0.38).roundToInt()
        val r = width - l
        val b = t + (height * 0.095).roundToInt()
        return Rect(l, t, r, b)
    }

    private val detector = FaceDetection.getClient(FaceDetectorOptions.Builder()
            .setPerformanceMode(FaceDetectorOptions.PERFORMANCE_MODE_ACCURATE)
            .setLandmarkMode(FaceDetectorOptions.LANDMARK_MODE_ALL)
            .setClassificationMode(FaceDetectorOptions.CLASSIFICATION_MODE_ALL)
            .setMinFaceSize(0.4f)
            .enableTracking()
            .build())

    private var faceExecutors = Executors.newSingleThreadExecutor()

    private var isFaceProcessing = false

    private var isPause = false

    private var isUpdateOverlay = true

    private var listener: FaceListener? = null

    fun initOverlay(graphicOverlayFace: GraphicOverlayFace?, graphicOverlayBG: GraphicOverlay?) {
        graphicOverlayFace?.setImageSourceInfo(SIZE.width, SIZE.height, false)
        graphicOverlayBG?.setImageSourceInfo(SIZE.width, SIZE.height, false)
        if (this.graphicOverlayFace == null) this.graphicOverlayFace = graphicOverlayFace
        if (this.graphicOverlayBG == null) this.graphicOverlayBG = graphicOverlayBG
    }

    fun updateOverlay(width: Int, height: Int) {
        if (!isUpdateOverlay) return
        graphicOverlayFace?.isUpdateImageSource = true
        graphicOverlayBG?.isUpdateImageSource = true
        trackingRect = null
        SIZE = Size(height, width)
        graphicOverlayFace?.setImageSourceInfo(SIZE.width, SIZE.height, false)
        graphicOverlayBG?.setImageSourceInfo(SIZE.width, SIZE.height, false)
        isUpdateOverlay = false
    }

    fun processImage(image: Image) {
        if (isPause) return
        if (isFaceProcessing) return
        isFaceProcessing = true
        val srcMat = image.yuvToRgba()
        if (srcMat.size().empty()) {
            isFaceProcessing = false
            return
        }
        updateOverlay(image.width, image.height)
        highAccuracyProcessing(srcMat, graphicOverlayFace)
    }

    fun processImageNV21(data: ByteArray, width: Int, height: Int) {
        if (isPause) return
        if (isFaceProcessing) return
        isFaceProcessing = true
        val srcMat = data.nv21ToMat(width, height)
        if (srcMat.size().empty()) {
            isFaceProcessing = false
            return
        }
        updateOverlay(width, height)
        highAccuracyProcessing(srcMat, graphicOverlayFace)
    }

    fun pause() {
        isPause = true
    }

    fun resume() {
        isPause = false
    }

    fun setListener(listener: FaceListener) {
        this.listener = listener
    }

    private fun highAccuracyProcessing(srcMat: Mat, graphicOverlay: GraphicOverlayFace?) {
        faceExecutors.submit {
            try {
                synchronized(isFaceProcessing) {
                    val raw = srcMat.clone()
                    val resized = srcMat.resizeFullFrame()?.rotate()?.toBitmap()
                    if (resized == null) {
                        isFaceProcessing = false
                        return@submit
                    }
                    srcMat.release()
                    val img = InputImage.fromBitmap(resized, 0)
                    detector.process(img)
                            .addOnCompleteListener { task ->
                                task.addOnSuccessListener { faces ->
                                    synchronized(isFaceProcessing) {
                                        clearFaceOverlay(graphicOverlay)
                                        val face = faces.getLargestFace()
                                        printInfoFace(face)
                                        val faceGraphic = addFaceOverlay(face, graphicOverlay)
                                        listener?.onFrame(face, raw.rotate(), checkFaceInside(faceGraphic), checkFaceOke(face))
                                    }

                                }.addOnFailureListener { e ->
                                    synchronized(isFaceProcessing) {
                                        clearFaceOverlay(graphicOverlay)
                                        listener?.onFrame()
                                        raw.release()
                                    }
                                }
                                resized.recycle()
                                isFaceProcessing = false
                            }
                }

            } catch (e: Exception) {
                e.printStackTrace()
                isFaceProcessing = false
            }
        }
    }

    private fun List<Face>.getLargestFace(): Face? {
        var largestFace: Face? = null
        if (this.isNotEmpty()) {
            this.forEach {
                if (largestFace == null) {
                    largestFace = it
                } else if (largestFace!!.boundingBox.width() < it.boundingBox.width()) {
                    largestFace = it
                }
            }
        }
        return largestFace
    }

    private fun Face?.checkSizeFace(): Boolean {
        this ?: return false
        return this.boundingBox.width() >= MIN_SIZE
    }

    private fun Face?.checkEulerFace(): Boolean {
        this ?: return false
        val x = this.headEulerAngleX
        val y = this.headEulerAngleY
        val z = this.headEulerAngleZ
//        Log.e("CheckFaceAngle", "X : $x - Y : $y")
        return x in ZONE_X && y in ZONE_Y && z in ZONE_Z
    }

    private fun printInfoFace(face: Face?) {
        face ?: return
//        Log.e("FaceInfo", "X:${face.headEulerAngleX} - Y:${face.headEulerAngleY} - Z:${face.headEulerAngleZ}")
//        Log.e("FaceInfo", "LeftEye: ${face.leftEyeOpenProbability} - RightEye: ${face.rightEyeOpenProbability}")
//        Log.e("FaceInfo", "Smile: ${face.smilingProbability}")
    }

    private fun addFaceOverlay(face: Face?, overlay: GraphicOverlayFace?): FaceGraphic? {
        face ?: return null
        overlay ?: return null
        val graphic = FaceGraphic(overlay, face)
        overlay.add(graphic)
        overlay.postInvalidate()
        return graphic
    }

    private fun clearFaceOverlay(overlay: GraphicOverlayFace?) {
        overlay ?: return
        overlay.clear()
        overlay.postInvalidate()
    }

    private fun checkFaceInside(face: FaceGraphic?): Boolean {
        face ?: return false
        return face.checkFaceInside()
    }

    private fun checkFaceOke(face: Face?): Boolean {
        face ?: return false
        return face.checkSizeFace() && face.checkEulerFace()
    }

    interface FaceListener {
        fun onFrame(face: Face? = null, bitmap: Mat? = null, isInside: Boolean = false, isOke: Boolean = false)
    }

}