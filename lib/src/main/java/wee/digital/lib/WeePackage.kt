package wee.digital.lib

import com.facebook.react.ReactPackage
import com.facebook.react.bridge.NativeModule
import com.facebook.react.bridge.ReactApplicationContext
import com.facebook.react.uimanager.ViewManager
import java.util.*

/**
 * -------------------------------------------------------------------------------------------------
 *
 * @Project: WeeLib
 * @Created: Huy 2021/05/05
 * @Organize: Wee Digital
 * @Description: ...
 * All Right Reserved
 * -------------------------------------------------------------------------------------------------
 */
class WeePackage : ReactPackage {

    override fun createViewManagers(reactContext: ReactApplicationContext): List<ViewManager<*, *>> {
        return emptyList()
    }

    override fun createNativeModules(
            reactContext: ReactApplicationContext,
    ): List<NativeModule> {
        val modules: MutableList<NativeModule> = ArrayList()
        modules.add(WeeModule(reactContext))
        return modules
    }

}