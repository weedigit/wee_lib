package wee.digital.lib

import android.content.Intent
import com.facebook.react.bridge.*
import faceotplib.BasicResp
import wee.digital.lib.ui.WeeFaceOtpActivity
import java.util.*

/**
 * -------------------------------------------------------------------------------------------------
 *
 * @Project: WeeLib
 * @Created: Huy 2021/05/05
 * @Organize: Wee Digital
 * @Description: ...
 * All Right Reserved
 * -------------------------------------------------------------------------------------------------
 */
class WeeModule(context: ReactApplicationContext?) : ReactContextBaseJavaModule(context) {

    override fun getName(): String {
        return "WeeModule"
    }

    override fun getConstants(): Map<String, Any>? {
        return HashMap()
    }

    @ReactMethod(isBlockingSynchronousMethod = false)
    fun enableSmartOTP(customerId: String?, pinCode: String?, callBack: Callback) {
        callBack.emit {
            val response = WeeLib.enableSmartOTP(customerId, pinCode)
            response.basicData.toMap()
        }
    }

    @ReactMethod(isBlockingSynchronousMethod = false)
    fun generateSmartOTP(customerId: String?, pinCode: String?, callBack: Callback) {
        callBack.emit {
            val response = WeeLib.generateSmartOTP(customerId, pinCode)
            response.basicData.toMap().also {
                it.putString("otp", response.smartOTP)
            }
        }
    }

    @ReactMethod(isBlockingSynchronousMethod = false)
    fun startFaceOtp(param: ReadableMap?, callBack: Callback?) {
        val intent = Intent(reactApplicationContext, WeeFaceOtpActivity::class.java)
        reactApplicationContext.startActivityForResult(intent, 1, null)
    }

    private fun Callback.emit(block: () -> WritableMap) {
        try {
            val map = block()
            invoke(map)
        } catch (throwable: Throwable) {
            invoke(throwable.toMap())
        }
    }

    private fun BasicResp.toMap(): WritableMap {
        return Arguments.createMap().also {
            it.putInt("code", java.lang.Long.valueOf(code).toInt())
            it.putInt("actionCode", java.lang.Long.valueOf(actionCode).toInt())
            it.putInt("actionIndex", java.lang.Long.valueOf(actionIndex).toInt())
            it.putString("message", message)
        }
    }

    private fun Throwable.toMap(): WritableMap {
        return Arguments.createMap().also {
            it.putInt("code", 1)
            it.putString("message", message)
        }
    }

}