package wee.digital.lib.ui

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.google.mlkit.vision.face.Face
import kotlinx.android.synthetic.main.face_otp.*
import org.opencv.core.Mat
import wee.digital.lib.R
import wee.digital.lib.face.util.Image.checkBrightness
import wee.digital.lib.face.util.Image.checkIfImageIsBlurred
import wee.digital.lib.face.util.Image.cropRect
import wee.digital.lib.face.util.Image.getRectRatio
import wee.digital.lib.face.util.Image.toBitmap
import wee.digital.lib.face.MLKitImp
import wee.digital.lib.face.MLKitImp.Companion.MAX
import wee.digital.lib.face.MLKitImp.Companion.MAXZ
import wee.digital.lib.face.MLKitImp.Companion.MIN
import wee.digital.lib.face.MLKitImp.Companion.MIN_BLUR
import wee.digital.lib.face.MLKitImp.Companion.ZONE_X
import wee.digital.lib.face.MLKitImp.Companion.ZONE_Y
import wee.digital.lib.face.MLKitImp.Companion.ZONE_Z
import wee.digital.lib.face.widget.GraphicOverlay
import wee.digital.lib.face.widget.GraphicOverlayFace
import wee.digital.lib.face.widget.CameraSource
import java.util.concurrent.Executors
import kotlin.math.abs

class WeeFaceOtpActivity : AppCompatActivity() , MLKitImp.FaceListener {

    data class FrameData(val face: Face? = null, val frame: Mat? = null, val brightness: Double = 0.0, val blurScore: Double = 0.0)

    private var mCameraSource: CameraSource? = null
    private val lock = Any()
    private var curI = -1
    private var curInZone = true
    private var curOke = false
    private var mHandler: Handler? = null
    private var curFaceID = -1
    private var frontFace = FrameData()
    private var deltaEyeL = -1f
    private var deltaEyeR = -1f
    private var curEyeL = -1f
    private var curEyeR = -1f
    private var curEyeLMax = -1f
    private var curEyeRMax = -1f
    private var curEyeLMin = 1f
    private var curEyeRMin = 1f
    private var curBrightness = 0f
    private val executors = Executors.newSingleThreadExecutor()
    private var isGetFrontFace = false
    private var message = "Invalid Face"
    private var maxBlur = 0.0
    private var minX = 110f
    private var minY = -110f
    private var minZ = 110f
    private var isPause = true
    private var isPermission = false

    private val runActionUI = Runnable {
        if (curInZone) {
            setUIInside()
            showMessage("DI CHUYỂN MẮT VÀO VÙNG NHẬN DIỆN NHÉ")

        } else {
            setUIOutside()
            showMessage("ĐỪNG NÔN NÓNG, SẼ XONG NGAY THÔI")
        }
        if (curInZone) {
            mHandler?.postDelayed(runShowResult, 1500)
        }
    }

    private val runActionResult = Runnable {
        val bmUserRaw = frontFace.frame?.toBitmap()
        isPause = true
        if (frontFace.face != null && bmUserRaw != null) {
            cameraPreview.pause()
            setUIResult()
        } else {
            val failedX = minX !in ZONE_X
            val isTooUp = minX > MAX
            val failedY = minY !in ZONE_Y
            val isTooLeft = minY < MIN
            val failedZ = minZ !in ZONE_Z
            val isTooRLeft = minZ > MAXZ
            val failedBlur = maxBlur < MIN_BLUR
            if (failedBlur) {
                showMessage("CHẤT LƯỢNG HÌNH ẢNH KHÔNG ĐẠT!!!")
            } else if (failedX) {
                if (isTooUp) {
                    showMessage("KHUÔN MẶT Ở GÓC QUÁ THẤP")
                } else {
                    showMessage("KHUÔN MẶT Ở GÓC QUÁ CAO")
                }
            } else if (failedY) {
                if (isTooLeft) {
                    showMessage("KHUÔN MẶT Ở GÓC QUÁ TRÁI")
                } else {
                    showMessage("KHUÔN MẶT Ở GÓC QUÁ PHẢI")
                }
            } else if (failedZ) {
                if (isTooRLeft) {
                    showMessage("KHUÔN MẶT BỊ XOAY VỀ BÊN TRÁI")
                } else {
                    showMessage("KHUÔN MẶT BỊ XOAY VỀ BÊN PHẢI")

                }
            }
            gifControl.show(true)
            mHandler?.postDelayed({
                resumeLayout()
            }, 1800)

        }
    }
    private val runShowResult = Runnable {
        showResultLayout()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.face_otp)

    }

    override fun onFrame(face: Face?, bitmap: Mat?, isInside: Boolean, isOke: Boolean) {
        if (isPause) return
        checkEyesZone(face, bitmap, isInside, isOke)
        gifControl.setRect(MLKitImp.instance.getRectTracking())
        lottieControl.setRect(MLKitImp.instance.getRectTracking())
    }

    private fun initGetFrameMLKit(graphicOverlayFace: GraphicOverlayFace?, graphicOverlayBG: GraphicOverlay?) {
        MLKitImp.instance.initOverlay(graphicOverlayFace, graphicOverlayBG)
        MLKitImp.instance.setListener(this)

    }

    private fun createCameraSource(graphicOverlayBG: GraphicOverlay?){

        if(!isPermission) return
        if (mCameraSource == null) {
            mCameraSource = CameraSource(this, graphicOverlayBG!!)
            mCameraSource!!.setFacing(CameraSource.CAMERA_FACING_FRONT)
        }


        mCameraSource!!.setFrameProcessorListener(object : CameraSource.FrameStreamListener {
            override fun onFrame(byteArray: ByteArray, width: Int, height: Int) {
                if(isPause) return
                MLKitImp.instance.processImageNV21(byteArray, width, height)
            }

        })
        cameraPreview?.start(mCameraSource)
        Log.e("Camera","createCameraSource")
    }

    private fun stopCameraResource(){
        mCameraSource?.release()
        mCameraSource = null
        cameraPreview?.release()
        Log.e("Camera","stopCameraResource")
    }


    private fun resetDeltaEye() {
        synchronized(lock) {
            curEyeL = -1f
            curEyeR = -1f
            deltaEyeR = -1f
            deltaEyeL = -1f
            curEyeLMax = -1f
            curEyeRMax = -1f
            curEyeLMin = 1f
            curEyeRMin = 1f
            message = ""
            maxBlur = 0.0
            minX = 110f
            minY = -110f
            minZ = 110f
            frontFace = FrameData()
        }

    }

    private fun getDeltaEyes(face: Face) {
        if (isFaceChange(face)) {
            resetDeltaEye()
        }
        val l = face.leftEyeOpenProbability
        val r = face.rightEyeOpenProbability
        var isFirst = false
        if (curEyeL == -1f) {
            curEyeL = l ?: -1f
            isFirst = true
        }
        if (curEyeR == -1f) {
            curEyeR = r ?: -1f
            isFirst = true
        }
        if (curEyeL == -1f || curEyeR == -1f || isFirst || l == null || r == null) return

        val dL = abs(curEyeL - l)
        val dR = abs(curEyeR - r)
        if (deltaEyeL < dL) deltaEyeL = dL
        if (deltaEyeR < dR) deltaEyeR = dR
        if (curEyeLMax < l) curEyeLMax = l
        if (curEyeRMax < r) curEyeRMax = r
        if (curEyeLMin > l) curEyeLMin = l
        if (curEyeRMin > r) curEyeRMin = r
//        Log.e("getDeltaEyes", "$deltaEyeL - $deltaEyeR - $curEyeLMin .. $curEyeLMax - $curEyeRMin .. $curEyeRMax")
    }

    private fun getFrontFace(face: Face?, frame: Mat?, isOke: Boolean) {
        face ?: return
        frame ?: return
        val openL = face.leftEyeOpenProbability ?: 0f
        val openR = face.rightEyeOpenProbability ?: 0f
        if (isGetFrontFace) return
        isGetFrontFace = true
        val rectFace = face.getRectRatio()
        // Check face & frame
        executors.submit {
            synchronized(lock) {
                val cropMat = frame.cropRect(rectFace)
                val brightness = checkBrightness(cropMat.clone())
                val blur = checkIfImageIsBlurred(cropMat.clone())
                if (maxBlur < blur || maxBlur == 0.0) maxBlur = blur
                val x = face.headEulerAngleX
                val y = face.headEulerAngleY
                val z = face.headEulerAngleZ
                if (abs(minX) > abs(x)) minX = x
                if (abs(minY) > abs(y)) minY = y
                if (abs(minZ) > abs(z)) minZ = z
                if (frontFace.blurScore < blur && blur > MIN_BLUR && openL > 0.5f && openR > 0.5f && isOke) {
                    frontFace = FrameData(face, cropMat, brightness, blur)
                }
                isGetFrontFace = false
            }

        }

    }

    private fun isFaceChange(face: Face?): Boolean {
        face ?: return true
        val faceId = face.trackingId ?: -1
        if (faceId != curFaceID) {
            curFaceID = faceId
            return true
        }
        return false
    }

    private fun checkEyesZone(face: Face?, frame: Mat?, isInside: Boolean, isOke: Boolean) {
        face ?: return
        curOke = isOke
        if (isInside != curInZone) {
            curInZone = isInside
            resetDeltaEye()
            mHandler?.removeCallbacks(runActionUI)
            mHandler?.removeCallbacks(runShowResult)
            mHandler?.postDelayed(runActionUI, 500)
        }

        if (isInside) {
            getDeltaEyes(face)
            getFrontFace(face, frame, isOke)
        }
        holeEyes.drawBG(curInZone, curOke)

    }

    private fun showResultLayout() {
        mHandler?.post(runActionResult)
    }

    private fun resumeLayout() {
        curInZone = false
        isGetFrontFace = false
        isPause = false
        gifControl.hide()
        lottieControl.hide()
        btnDone.visibility = View.GONE
        lottieControl.clear()
        holeEyes.showBG()
        if (curInZone) setUIInside() else setUIOutside()
        mHandler?.post(runActionUI)
    }


    override fun onPause() {
        isPause = true
        super.onPause()
    }

    override fun onStop() {
        stopCameraResource()
        super.onStop()
    }

    private fun setUIInside() {
        gifControl.show(false)
        lottieControl.hide()
    }

    private fun setUIOutside() {
        gifControl.hide()
        lottieControl.hide()
        holeEyes.isActionDone = false
    }

    private fun setUIResult() {
        txtMessage.text = null
        gifControl.hide()
        lottieControl.show()
        imgBG.setBackgroundResource(android.R.color.white)
        imgLogo.setImageResource(android.R.color.white)
        lottieControl.playAnimation {
            btnDone.visibility = View.VISIBLE
        }
        holeEyes.hideBG()
    }

    private fun adjustScreenBrightness(brightness: Float) {
        this.also {
            val attr = it.window.attributes
            curBrightness = attr.screenBrightness
            attr.screenBrightness = brightness
            it.window.attributes = attr
        }
    }

    private fun rollBackScreenBrightness() {
        this.also {
            val attr = it.window.attributes
            attr.screenBrightness = curBrightness
            it.window.attributes = attr
        }
    }

    private fun showMessage(message: String?) {
        txtMessage.post {
            txtMessage.text = message
        }
    }
    override fun onResume() {
        adjustScreenBrightness(1f)
        resumeLayout()
        createCameraSource(holeEyes)
        super.onResume()
    }

}