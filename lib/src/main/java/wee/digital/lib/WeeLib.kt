package wee.digital.lib

import android.app.Application
import android.os.Handler
import android.os.Looper
import android.widget.Toast
import faceotplib.*
import java.io.File
import java.util.*

/**
 * -------------------------------------------------------------------------------------------------
 * @Project: WeeLib
 * @Created: Huy 2021/05/07
 * @Organize: Wee Digital
 * @Description: ...
 * All Right Reserved
 * -------------------------------------------------------------------------------------------------
 */
object WeeLib {

    lateinit var app: Application
        private set

    fun initModule(application: Application) {
        //OpenCV.init(application)
        app = application
    }

    val instance: Lib by lazy {
        val keyService: KeyService = object : KeyService {
            override fun getDataWithKey(s: String): ByteArray {
                return ByteArray(0)
            }

            override fun setDataWithKey(s: String, bytes: ByteArray) {}
        }
        val appDir = app.filesDir
        val libDir = File(appDir, "faceotp")
        libDir.mkdir()
        val path: String = libDir.toString()
        Faceotplib.newLib(1L, keyService, path)

    }

    fun toast(format: String?, vararg args: Any?) {
        val s = Formatter().format(format, *args).toString()
        if (Looper.myLooper() == Looper.getMainLooper()) {
            Toast.makeText(app, s, Toast.LENGTH_SHORT).show()
            return
        }
        Handler(Looper.getMainLooper()).post {
            Toast.makeText(app, s, Toast.LENGTH_SHORT).show()
        }
    }

    fun enableSmartOTP(customerId: String?, pinCode: String?): EnableSmartOTPResp {
        return instance
                .smartOTPService
                .enableSmartOTP(customerId, pinCode)
    }

    fun generateSmartOTP(customerId: String?, pinCode: String?): GenerateSmartOTPResp {
        return instance
                .smartOTPService
                .generateSmartOTP(customerId, pinCode)
    }

}