## OTP Navite module:

Wee OTP native module include 

## Android install: 



In react navite project directory append this lines in file **../android/build.gradle**:

In buildscript:
```gradle
buildscript {
    //...
    dependencies {
        /...
        classpath 'de.undercouch:gradle-download-task:4.1.1'
    }
}
```

And at the end file:
```gradle
apply plugin: 'de.undercouch.download'

task downloadZipFile(type: Download) {
    src 'https://raw.githubusercontent.com/wee-digital/ExZip/master/Pack.zip'
    dest "$projectDir/Pack.zip"
}

task downloadAndUnzipFile(dependsOn: downloadZipFile, type: Copy) {
    from zipTree(downloadZipFile.dest)
    into "$projectDir"
}
```

The press arrow button to run task, it'll download module and unzip:
![picture](1.png)

